---
layout: page
title: Who writes all this?!
tags: [about, gem]
modified: 2018-18-08T20:53:07.573882-04:00
comments: true
image:
  feature:
---

<h3>My Name: Nandaja</h3>

I can tell you one thing that even Google doesn't know: The meaning of my name.
<br/><br/>So here is some background...

According to the Hindu mythology, The Hindu lord Krishna was born in a prison cell when his parents
were imprisoned by his uncle. Since it was prophecied that he will grow up to be a prodigy, considering the possible life threat he has in cell, he was secretly taken out by his Foster parent Nanda who leaves his own daughter in Krishna's place so the evil uncle will not be suspicious.

The evil uncle who hears about his sister's new baby comes for the visit, finds the baby daughter
and decides to kill her. Upon which, the girl transforms into a goddess and warns Kamhsa, the uncle,
that the prodigy has already been born and will be the reason for Kamhsa's end when he comes of age.
She then vanishes leaving an awestruck Kamhsa behind.<br/>
This goddess who was daughter to Nanda was called as Nanda-ja(ja meaning the one who was born from).
<br/>
She is popularly referred by the name Maya meaning *Illusion* or a godly power that can transform a
concept into an element of sensible world.
<br/>
Now that meaning, I like. :-)
<br/><br/>
In conclusion, *<b>Nandaja</b> is someone who is destined to transform the random thoughts into elements of the sensible world!*
<br/><br/>[*Now even Google knows the meaning of my name*]



<h3>About me:</h3>


I am a software developer by both profession and interest. Finds home coding in Python.
<br/>
Technically actions speak louder than words, so move along and check out my blog!
<br/>


<h3>What am I upto these days:</h3>


I am a Masters' student now. Don't ask why. I live in Southern Europe
now. PM me if you want to catch up! I work part-time and the rest of the
time I travel. :-)
